<h1 id="brew">brew</h1>
<p>see all homebrew-installed packages</p>
<pre><code>brew list</code></pre>
<p>search for an installable package containing the string ack</p>
<pre><code>brew search ack</code></pre>
<p>install the ack package</p>
<pre><code>brew install ack</code></pre>
<p>upgrade an installed node to the most recent version</p>
<pre><code>brew upgrade node</code></pre>
<p>see all installed versions of node</p>
<pre><code>brew info node</code></pre>
<p>use the installed version 0.10.20 of node instead of the current version</p>
<pre><code>brew switch node 0.10.20</code></pre>
<h1 id="basic-usage">Basic Usage</h1>
<p><code>brew</code> is the command for Homebrew, a package manager for OSX. It is most commonly used to install a package or utility:</p>
<pre><code>brew install &lt;package&gt;</code></pre>
<p>Search for a formula containing a string:</p>
<pre><code>brew search &lt;string&gt;</code></pre>
<h1 id="installing-things">Installing Things</h1>
<p>The people maintaining Homebrew curate a list of software that can be installed using <code>brew</code>. The instructions for installing a particular package are called a formula. Before installing things, it is good practice to make sure you have the most up to date version of all formulae by running <code>update</code>:</p>
<pre><code>brew update</code></pre>
<p>Once you are dealing with the most recent formulae, install things by giving <code>brew</code> the <code>install</code> command and a package name. To see if <code>node</code> can be installed using <code>brew</code>, first search all formulae to see if Homebrew offers an install of <code>node</code>:</p>
<pre><code>brew search node</code></pre>
<p>Homebrew will print all matching formulae. <code>node</code> is among them, so install it:</p>
<pre><code>brew install node</code></pre>
<p>After a package is installed, watch the output for any printed caveats. These are formula-dependent. Homebrew is good about taking care of things for you, but sometimes you’ll still have to handle something yourself.</p>
<h2 id="what-does-install-do">What Does Install Do?</h2>
<p><code>brew install</code> does two things. First, it somehow gets an executable onto your machine. It might download an executable directly (which is referred to as a “bottle”), or it might build from source. Second, it adds symlinks to these executables so that they will be on your path.</p>
<p>By default, <code>brew</code> installs things into your Cellar. The location of your Cellar can be found by:</p>
<pre><code>brew --cellar</code></pre>
<p>Inside your Cellar, programs are stored according to their package name and version. This will show where Homebrew installs <code>node</code> inside your Cellar:</p>
<pre><code>$ brew --cellar node
/usr/local/Cellar/node</code></pre>
<p>Inside that directory are other directories with all the versions of the <code>node</code> package installed on your machine:</p>
<pre><code>$ ls /usr/local/Cellar/node
0.10.20/  0.10.36/  0.12.2_1/</code></pre>
<p>After the executable is available, it lives in the Cellar in one of the above directories. Homebrew then creates symlinks to the executable as well as other relevant files like man pages in a place that is on your path. Exactly where these symlinks live varies depending on the formula, but they usually live at <code>$(brew --prefix)/bin</code>.</p>
<h1 id="linking">Linking</h1>
<p>A keg can be installed without being symlinked on your path. This gives you more flexibility, as you can essentially deactivate a formula without having to manually curate your path or delete the installed the executable.</p>
<p>This will remove the symlinks to <code>node</code>:</p>
<pre><code>brew unlink node</code></pre>
<p>An installed, unlinked formula is referred to as “keg only”. To create symlinks to the formula, run the <code>link</code> command. This will create symlinks to <code>node</code>:</p>
<pre><code>brew link node</code></pre>
<h1 id="taps">Taps</h1>
<p>A tap is a list of formulae that extends the default list provided by Homebrew. All taps are described as a two part name containing a single slash. For example, <code>homebrew/versions</code> is a list of formulae that allow installation of specific versions of software.</p>
<p>This naming convention refers to a repository on Github and is of the format: <code>&lt;username&gt;/hombrew-&lt;name&gt;</code>. <code>homebrew/versions</code> is therefore shorthand for <code>homebrew/homebrew-versions</code> and the source for the tap can be found on Github at <code>https://github.com/homebrew/homebrew-versions</code>.</p>
<p>See your active taps:</p>
<pre><code>brew tap</code></pre>
<p>Activate the <code>homebrew/versions</code> tap:</p>
<pre><code>brew tap homebrew/versions</code></pre>
<p>Deactivate the <code>homebrew/versions</code> tap:</p>
<pre><code>brew untap homebrew/versions</code></pre>
<h1 id="versions">Versions</h1>
<p>Kegs for multiple versions of a single formula can exist simultaneously in the Cellar.</p>
<p>See all installed versions of the node formula:</p>
<pre><code>brew info node</code></pre>
<p>Switch to a different installed version of a node:</p>
<pre><code>brew switch node 0.12.20</code></pre>
<p>If you want to install an older version of a tool you’ll likely need the <code>homebrew/versions</code> tap. If you have previously installed a newer version of the tool, you’ll need to first unlink that version. This sequence of commands assumes that you have installed <code>node</code> greater than version <code>010</code> but want to install <code>010</code>:</p>
<pre><code>$ brew tap homebrew/versions
$ brew unlink node
$ brew search node
homebrew/versions/node010    homebrew/versions/node08   nodebrew
hombrew/versions/node04      leafnode                   modenv
homebrew/versions/node06     node
$ brew install homebrew/versions/node010</code></pre>
<h1 id="casks">Casks</h1>
<p>A Cask is a <code>.app</code> style application that can be installed using Homebrew. This is an alternative to downloading a <code>.dmg</code> file and dragging an icon into your <code>Applications/</code> directory.</p>
<p>Install the <code>cask</code> command:</p>
<pre><code>brew install caskroom/cask/brew-cask</code></pre>
<p>Search for casks containing the string <code>chrome</code>:</p>
<pre><code>brew cask search chrome</code></pre>
<p>Install the <code>google-chrome</code> cask:</p>
<pre><code>brew cask install google-chrome</code></pre>
<p>Print the <code>cask</code> help:</p>
<pre><code>brew cask help</code></pre>
<h1 id="terminology">Terminology</h1>
<p>Homebrew’s strict adherence to beer-related terms can lead to some confusion.</p>
<h2 id="formula">Formula</h2>
<p>The definition of a package. This is a ruby file that lives at <code>/usr/local/Libary/Formula</code>.</p>
<p>See the formula for <code>node</code>:</p>
<pre><code>less /usr/local/Library/Formula/node.rb</code></pre>
<h2 id="keg">Keg</h2>
<p>The installation prefix of a formula. This can also be thought of as a particular version of a package. A <code>node</code> keg for version <code>0.10.20</code> might be <code>/usr/local/Cellar/node/0.10.20</code>.</p>
<p>This indicates there are three <code>node</code> kegs on the machine:</p>
<pre><code>$ ls /usr/local/Cellar/node
0.10.20/  0.10.36/  0.12.2_1/</code></pre>
<h2 id="cellar">Cellar</h2>
<p>Where kegs are installed. Find the location with:</p>
<pre><code>brew --cellar</code></pre>
<h2 id="bottle">Bottle</h2>
<p>A binary keg that can be unpacked. These frequently end in <code>.tar.gz</code> and are referred to as “poured from bottle” or “bottled” when running <code>brew info</code> about a particular package:</p>
<pre><code>brew info node</code></pre>
<h2 id="tap">Tap</h2>
<p>A list of additional optional formulae that can be used to extend Homebrew’s default list.</p>
<h2 id="cask">Cask</h2>
<p>A <code>.app</code> style program that can be installed using Homebrew.</p>
